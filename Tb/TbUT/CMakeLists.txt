################################################################################
# Package: TbUT
################################################################################
gaudi_subdir(TbUT v1r2)

gaudi_depends_on_subdirs(Tb/TbEvent
                         Tb/TbKernel
                         GaudiAlg)

find_package(ROOT COMPONENTS Minuit MathCore GenVector)
find_package(Boost COMPONENTS iostreams)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(TbUT
                 src/*.cpp
                 src/alibava/*.cpp
                 src/mamba/*.cpp
                 LINK_LIBRARIES TbEventLib TbKernelLib GaudiAlgLib Boost ROOT)

