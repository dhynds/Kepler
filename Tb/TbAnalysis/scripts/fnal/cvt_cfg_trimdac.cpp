// void cvt_cfg_trimdac(char *cfg_file, std::string txt_file) {
void cvt_cfg_trimdac(const std::string& cfg_file, const std::string& txt_file) {
    char arr[256*256];
    int trim, mask, tp_ena;

    FILE *fp = fopen( cfg_file.c_str(), "rb");
    if (fp == NULL) { 
      cout << "can not open file: " << cfg_file << endl; 
      return;
    }
    fread(arr, sizeof(char), 256*256, fp); 

    std::ofstream outfile(txt_file.c_str(), std::ofstream::out); 
    // cout << "# col  row trim mask tp_ena" << endl;
    outfile << "# col  row trim mask tp_ena" << endl;
    for (int col=0; col<256; ++col) {
        for (int row=0; row<256; ++row) {
            int idx = 256*row + col;
            trim = arr[idx] & 0xF;
            mask = (arr[idx] >>4) & 0x1;
            tp_ena = (arr[idx]>>5) & 0x1;
            // cout << setw(5) <<  col <<
            outfile << setw(5) <<  col <<
                    setw(5) << row <<
                    setw(5) << trim <<
                    setw(5) << mask <<
                    setw(5) << tp_ena << endl;
       }
   }
   outfile.close();
}
