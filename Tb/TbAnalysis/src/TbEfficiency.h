#ifndef TB_EFFICIENCY_H
#define TB_EFFICIENCY_H 1

#include "AIDA/IHistogram1D.h"
#include "TH2.h"
#include "TEfficiency.h"

// Tb/TbKernel
#include "TbKernel/TbAlgorithm.h"

/** @class TbEfficiency TbEfficiency.h
 *
 */

class TbEfficiency : public TbAlgorithm {
 public:
  /// Constructor
  TbEfficiency(const std::string& name, ISvcLocator* pSvcLocator);
  /// Destructor
  virtual ~TbEfficiency() {}

  virtual StatusCode initialize() override;  ///< Algorithm initialization
  virtual StatusCode execute() override;     ///< Algorithm execution
  virtual StatusCode finalize() override;

 private:
  bool m_checkHitDUT;
  bool m_checkHitAlivePixel;
  double m_pointingResAllowance;
  unsigned int m_nTracksConsidered;
  unsigned int m_nTracksAssociated;
  unsigned int m_event;
  int m_pointingResAllowance_deadPixels;
  bool m_takeDeadPixelsFromFile;
  TH2F* m_deadPixelMap;
  double m_pitch;
  unsigned int m_nTotalTracks;
  double m_maxChi;

  bool passedThroughDUT(const Gaudi::XYZPoint& interceptUL) const;
  bool passedThroughAlivePixel(const Gaudi::XYZPoint& interceptUL) const;

  bool passSelectionCriteria(const LHCb::TbTrack& track, const Gaudi::XYZPoint& interceptUL) const;

  TEfficiency* h_row;
  TEfficiency* h_col;
  TEfficiency* m_eff;
  TEfficiency* h_hitmap;
  TEfficiency* h_pixel;
  TEfficiency* h_pixel2x2;

  unsigned int m_dut;
  std::string m_trackLocation;
  std::string m_clusterLocation;
};
#endif
