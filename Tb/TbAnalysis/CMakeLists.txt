################################################################################
# Package: TbAnalysis
################################################################################
gaudi_subdir(TbAnalysis v1r2)

gaudi_depends_on_subdirs(Tb/TbEvent
                         Tb/TbKernel
                         GaudiAlg)

find_package(ROOT COMPONENTS MathCore GenVector Physics Matrix)
find_package(Boost COMPONENTS iostreams)

include_directories(SYSTEM ${ROOT_INCLUDE_DIRS})

gaudi_add_module(TbAnalysis
                 src/*.cpp
                 LINK_LIBRARIES TbEventLib TbKernelLib GaudiAlgLib Boost ROOT)

