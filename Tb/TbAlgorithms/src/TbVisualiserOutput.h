#ifndef TB_VISUALISEROUTPUT_H
#define TB_VISUALISEROUTPUT_H 1

// Tb/TbKernel
#include "TbKernel/ITbClusterFinder.h"
#include "TbKernel/TbAlgorithm.h"

// Tb/TbEvent
#include "Event/TbTrack.h"

/** @class TbVisualiserOutput TbVisualiserOutput.h
 * @author Dan Saunders
 */

class TbVisualiserOutput : public TbAlgorithm {
 public:
  /// Constructor
  TbVisualiserOutput(const std::string &name, ISvcLocator *pSvcLocator);
  /// Destructor
  virtual ~TbVisualiserOutput();

  virtual StatusCode initialize() override;  ///< Algorithm initialization
  virtual StatusCode execute() override;     ///< Algorithm execution

 private:
  ITbClusterFinder *m_clusterFinder;
  std::string m_clusterLocation;
  std::string m_trackLocation;

  unsigned int m_viewerEvent;
  unsigned int m_event;

  void outputViewerData(const LHCb::TbTracks *tracks);
};
#endif
