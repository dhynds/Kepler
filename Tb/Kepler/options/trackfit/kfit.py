from Gaudi.Configuration import *

trackFitToolName = "TbKalmanTrackFit"
from Configurables import TbKalmanTrackFit
s2 = 6.e-11
sigmas = [s2, s2, s2, s2, 0., s2, s2, s2, s2]
from Configurables import TbSimpleTracking
TbSimpleTracking().addTool(TbKalmanTrackFit, name = "Fitter")
TbSimpleTracking().Fitter.Noise2 = sigmas
TbSimpleTracking().Fitter.Noise2Default = s2
TbSimpleTracking().TrackFitTool = trackFitToolName
from Configurables import TbTrackPlots
# TbTrackPlots().TrackFitTool = trackFitToolName

from Configurables import TbTracking
# TbTracking().TrackFitTool = trackFitToolname
from Configurables import TbVertexTracking
# TbVertexTracking().TrackFitTool = trackFitToolName

from Configurables import TbAlignment
# TbAlignment().TrackFitTool = trackFitToolName
