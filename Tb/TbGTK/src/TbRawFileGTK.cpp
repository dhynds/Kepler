// Local
#include "TbRawFileGTK.h"

//=============================================================================
// Standard constructor
//=============================================================================
TbRawFileGTK::TbRawFileGTK(const std::string& filename/*,
                     TbHeaderDecoder* headerDecoder*/)
    : TbBufferedFile(filename, 0) {

  if (!is_open()) {
    m_good = false;
    return;
  }

}
