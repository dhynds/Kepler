#ifndef __TDCPixTimeStamp__HH__
#define __TDCPixTimeStamp__HH__

#define TDCPIX_TS_BUFFER_SIZE 6

#include <iostream>
#include <vector>

namespace GTK {

class TDCPixTimeStamp {
 public:
  TDCPixTimeStamp(int serial_fragment = 0);
  ~TDCPixTimeStamp();
  static const double ClockPeriod;
  int Clear();

  std::pair<unsigned int, double> GetLeadingTime(const double offset = 0., 
                                                 const bool fCorr = true) const;
  double GetTrailingTime(const bool fCorr = true) const;

  unsigned short IsTimeStamp() const {
    // bit 47: 1 => frame synch word; 0 => time stamp 
    return (mBuf[5] & 0x80) ? 0 : 1;
  }
  unsigned short IsPaddingWord() const;

  /// Time stamp data accessors
  unsigned short GetTrailingFineTime() const;
  unsigned short GetTrailingCoarseTime() const;
  unsigned short GetTrailingCoarseTimeSelector() const;

  unsigned short GetLeadingFineTime() const;
  unsigned short GetLeadingCoarseTime() const;
  unsigned short GetLeadingCoarseTimeSelector() const;

  unsigned short GetHitAddress() const;
  unsigned short GetPileUpAddress() const;

  unsigned short GetPixelGroupAddress() const;
  unsigned short GetQChip() const { return mQChip; }
  void SetQChip(const unsigned short qchip) { mQChip = qchip; }

  unsigned short GetGTK() const { return mGTK; }
  void SetGTK(const unsigned short gtk) { mGTK = gtk; }

  unsigned short GetChipId() const { return mChipId; }
  void SetChipId(const unsigned short chipid) { mChipId = chipid; }

  unsigned int GetNaturalPixelIndex() const;
  unsigned int GetNaturalColumnIndex() const;
  int GetPixelUID() const;

  unsigned char Get6bitWord(unsigned int idx) const;

  const unsigned char* GetBuffer() const  { return mBuf; }
  int SetBuffer(const unsigned char* buf);

  void SetFrameCount(const unsigned int fc) { mFrameCount = fc; }
  unsigned int GetFrameCount() const;

  friend std::ostream& operator<<(std::ostream& os, const TDCPixTimeStamp& ts);

 private:
  unsigned short mQChip = 0;
  unsigned short mChipId;
  unsigned short mGTK = 0;
  int mIsSerialFragment = 0;
  unsigned int mFrameCount = 0;
  unsigned char mBuf[TDCPIX_TS_BUFFER_SIZE];
};

std::ostream& operator<<(std::ostream& os, const TDCPixTimeStamp& ts);

std::ostream& operator<<(std::ostream& os,
                         const std::vector<TDCPixTimeStamp>& vts);

}

#endif
