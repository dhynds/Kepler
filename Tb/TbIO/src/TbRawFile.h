#pragma once

// Tb/TbKernel
#include "TbKernel/TbBufferedFile.h"

// Local
#include "TbHeaderDecoder.h"

/** @class TbRawFile TbRawFile.h
 *
 * Interface for raw files in SPIDR format via TbBufferedFile (ROOT I/O)
 *
 */

class TbRawFile : public TbBufferedFile<1000000, uint64_t> {
 public:
  /// Constructor
  TbRawFile(const std::string& filename, TbHeaderDecoder* headerDecoder);
  /// Destructor
  virtual ~TbRawFile() {}

  /// Return the file index
  unsigned int splitIndex() const { return m_split_index; }
  /// Return the chip identifier
  std::string deviceId() const { return m_deviceId; }
  /// Return the device type
  unsigned int deviceType() const { return m_deviceType; }
  /// Return whether the file has been opened successfully
  bool good() const { return m_good; }
  /// Return the number of data packets in the file
  uint64_t nPackets() const { return m_nPackets; }

  /// Set the offset in size of packets
  virtual void setOffset(const uint64_t offset) {
    TbBufferedFile::setOffset(offset * m_packetSize + m_headerSize);
  }

 protected:
  /// Number of data packets in the file
  uint64_t m_nPackets = 0;
  /// Index by which large files are divided
  unsigned int m_split_index = 1;
  /// Packet size in bytes
  unsigned int m_packetSize = 8;
  /// Header size in bytes
  uint32_t m_headerSize = 0;
  /// Chip type
  unsigned int m_deviceType = 0;
  /// Chip identifier
  std::string m_deviceId;
  bool m_good = true;
};
